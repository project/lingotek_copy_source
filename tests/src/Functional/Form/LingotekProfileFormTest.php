<?php

namespace Drupal\Tests\lingotek_copy_source\Functional\Form;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\lingotek\Entity\LingotekProfile;
use Drupal\lingotek_test\Controller\FakeAuthorizationController;
use Drupal\Tests\lingotek\Functional\Form\IntelligenceMetadataFormTestTrait;
use Drupal\Tests\lingotek\Functional\LingotekTestBase;

/**
 * Tests the Lingotek profile form.
 *
 * @group lingotek_copy_source
 */
class LingotekProfileFormTest extends LingotekTestBase {

  use IntelligenceMetadataFormTestTrait;

  /**
   * {@inheritdoc}
   *
   * Use 'classy' here, as we depend on that for querying the selects in the
   * target overriddes class.
   *
   * @see testProfileSettingsOverride()
   */
  protected $defaultTheme = 'classy';

  /**
   * {@inheritdoc}
   */
  public static $modules = ['node', 'lingotek_copy_source'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setupResources();
  }

  /**
   * Test adding a profile are present.
   */
  public function testAddingProfile() {
    $assert_session = $this->assertSession();

    ConfigurableLanguage::createFromLangcode('es')->setThirdPartySetting('lingotek', 'locale', 'es_MX')->save();
    ConfigurableLanguage::createFromLangcode('it')->setThirdPartySetting('lingotek', 'locale', 'it_IT')->save();

    $this->drupalGet('admin/lingotek/settings');
    $this->clickLink(t('Add new Translation Profile'));

    $profile_id = strtolower($this->randomMachineName());
    $profile_name = $this->randomString();
    $edit = [
      'id' => $profile_id,
      'label' => $profile_name,
      'auto_upload' => 1,
      'auto_download' => 1,
      'append_type_to_title' => 'yes',
      'language_overrides[es][overrides]' => 'custom',
      'language_overrides[es][custom][auto_download]' => FALSE,
      'language_overrides[es][custom][workflow]' => 'test_workflow',
      'language_overrides[it][overrides]' => 'copy_source',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));

    $this->assertText(t('The Lingotek profile has been successfully saved.'));

    // We can edit them.
    $this->assertLinkByHref("/admin/lingotek/settings/profile/$profile_id/edit");
    $this->drupalGet("/admin/lingotek/settings/profile/$profile_id/edit");

    $assert_session->optionExists('edit-language-overrides-es-overrides', 'custom');
    $assert_session->optionExists('edit-language-overrides-en-overrides', 'default');
    $assert_session->optionExists('edit-language-overrides-it-overrides', 'copy_source');
  }

  /**
   * Test profiles language settings override.
   */
  public function testEditingProfile() {
    $assert_session = $this->assertSession();

    // Add a language.
    ConfigurableLanguage::createFromLangcode('es')->setThirdPartySetting('lingotek', 'locale', 'es_MX')->save();
    ConfigurableLanguage::createFromLangcode('it')->setThirdPartySetting('lingotek', 'locale', 'it_IT')->save();

    /** @var \Drupal\lingotek\LingotekProfileInterface $profile */
    $profile = LingotekProfile::create([
      'id' => strtolower($this->randomMachineName()),
      'label' => $this->randomString(),
    ]);
    $profile->save();
    $profile_id = $profile->id();

    $this->drupalGet("/admin/lingotek/settings/profile/$profile_id/edit");
    $assert_session->optionExists('edit-language-overrides-es-overrides', 'default');
    $assert_session->optionExists('edit-language-overrides-en-overrides', 'default');
    $assert_session->optionExists('edit-language-overrides-it-overrides', 'default');

    $edit = [
      'auto_upload' => FALSE,
      'auto_download' => 1,
      'project' => 'default',
      'vault' => 'default',
      'workflow' => 'test_workflow2',
      'language_overrides[es][overrides]' => 'custom',
      'language_overrides[es][custom][auto_download]' => FALSE,
      'language_overrides[es][custom][workflow]' => 'test_workflow',
      'language_overrides[it][overrides]' => 'copy_source',
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));

    $this->drupalGet("/admin/lingotek/settings/profile/$profile_id/edit");
    $this->assertNoFieldChecked("edit-auto-upload");
    $this->assertFieldChecked("edit-auto-download");
    $assert_session->optionExists('edit-project', 'default');
    $assert_session->optionExists('edit-vault', 'default');
    $assert_session->optionExists('edit-workflow', 'test_workflow2');
    $assert_session->optionExists('edit-language-overrides-es-overrides', 'custom');
    $assert_session->optionExists('edit-language-overrides-en-overrides', 'default');
    $assert_session->optionExists('edit-language-overrides-it-overrides', 'copy_source');
  }

  /**
   * Setup test resources for the test.
   */
  protected function setupResources() {
    $config = \Drupal::configFactory()->getEditable('lingotek.account');
    $config->set('resources.community', [
      'test_community' => 'Test community',
      'test_community2' => 'Test community 2',
    ]);
    $config->set('resources.project', [
      'test_project' => 'Test project',
      'test_project2' => 'Test project 2',
    ]);
    $config->set('resources.vault', [
      'test_vault' => 'Test vault',
      'test_vault2' => 'Test vault 2',
    ]);
    $config->set('resources.workflow', [
      'test_workflow' => 'Test workflow',
      'test_workflow2' => 'Test workflow 2',
    ]);
    $config->set('resources.filter', [
      'test_filter' => 'Test filter',
      'test_filter2' => 'Test filter 2',
      'test_filter3' => 'Test filter 3',
    ]);
    $config->set('access_token', FakeAuthorizationController::ACCESS_TOKEN);

    $config->set('default.community', 'test_community');
    $config->set('default.workflow', 'test_workflow');
    $config->set('default.project', 'test_project');
    $config->set('default.vault', 'test_vault');
    $config->save();
  }

}
