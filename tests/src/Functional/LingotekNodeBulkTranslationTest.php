<?php

namespace Drupal\Tests\lingotek_copy_source\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\lingotek\Entity\LingotekProfile;
use Drupal\lingotek\Lingotek;
use Drupal\Tests\lingotek\Functional\LingotekTestBase;

/**
 * Tests translating a node using the bulk management form.
 *
 * @group lingotek_copy_source
 */
class LingotekNodeBulkTranslationTest extends LingotekTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['block', 'node', 'lingotek_copy_source'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    // Create Article node types.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Add a couple of languages.
    ConfigurableLanguage::createFromLangcode('es')
      ->setThirdPartySetting('lingotek', 'locale', 'es_MX')
      ->save();
    ConfigurableLanguage::createFromLangcode('it')
      ->setThirdPartySetting('lingotek', 'locale', 'it_IT')
      ->save();

    $profile = LingotekProfile::create([
      'id' => 'custom_profile',
      'label' => 'Custom Profile',
      'auto_upload' => FALSE,
      'auto_request' => FALSE,
      'auto_download' => FALSE,
      'auto_download_worker' => FALSE,
      'language_overrides' => [
        'es' => [
          'overrides' => 'custom',
          'custom' => [
            'auto_request' => FALSE,
            'auto_download' => FALSE,
          ],
        ],
        'it' => [
          'overrides' => 'copy_source',
        ],
      ],
    ]);
    $profile->save();

    // Enable translation for the current entity type and ensure the change is
    // picked up.
    ContentLanguageSettings::loadByEntityTypeBundle('node', 'article')
      ->setLanguageAlterable(TRUE)
      ->save();
    \Drupal::service('content_translation.manager')
      ->setEnabled('node', 'article', TRUE);

    drupal_static_reset();
    \Drupal::entityTypeManager()->clearCachedDefinitions();
    $this->applyEntityUpdates();
    // Rebuild the container so that the new languages are picked up by services
    // that hold a list of languages.
    $this->rebuildContainer();

    $this->saveLingotekContentTranslationSettingsForNodeTypes(['article'], 'custom_profile');
  }

  /**
   * Tests that a node can be translated using the links on the management page.
   */
  public function testNodeTranslationUsingLinks() {
    // Login as admin.
    $this->drupalLogin($this->rootUser);

    // Create a node.
    $edit = [];
    $edit['title[0][value]'] = 'Llamas are cool';
    $edit['body[0][value]'] = 'Llamas are very cool';
    $edit['langcode[0][value]'] = 'en';
    $edit['lingotek_translation_management[lingotek_translation_profile]'] = 'custom_profile';
    $this->saveAndPublishNodeForm($edit);

    // Login as translation manager.
    $this->drupalLogin($this->translationManagerUser);

    $this->goToContentBulkManagementForm();

    // Clicking English must init the upload of content.
    $this->assertLingotekUploadLink();
    // And we cannot request yet a translation.
    $this->assertNoLingotekRequestTranslationLink('es_MX');
    $this->assertNoLingotekRequestCopyTranslationLink('it_IT');

    $this->clickLink('EN');
    $this->assertText('Node Llamas are cool has been uploaded.');
    $this->assertIdentical('en_US', \Drupal::state()
      ->get('lingotek.uploaded_locale'));

    // There is a link for checking status.
    $this->assertLingotekCheckSourceStatusLink();
    // And we can already request a translation.
    $this->assertLingotekRequestTranslationLink('es_MX');

    // But not for Italian, that has the special setting and points to a different
    // endpoint.
    $this->assertNoLingotekRequestTranslationLink('it_IT');
    $this->assertLingotekRequestCopyTranslationLink('it_IT');

    $this->clickLink('EN');
    $this->assertText('The import for node Llamas are cool is complete.');

    // Request the Spanish translation.
    $this->assertLingotekRequestTranslationLink('es_MX');
    $this->clickLink('ES');
    $this->assertText("Locale 'es_MX' was added as a translation target for node Llamas are cool.");
    $this->assertIdentical('es_MX', \Drupal::state()
      ->get('lingotek.added_target_locale'));

    // Check status of the Spanish translation.
    $this->assertLingotekCheckTargetStatusLink('es_MX');
    $this->clickLink('ES');
    $this->assertIdentical('es_MX', \Drupal::state()
      ->get('lingotek.checked_target_locale'));
    $this->assertText('The es_MX translation for node Llamas are cool is ready for download.');

    // Request the Italian translation copy.
    $this->assertLingotekRequestCopyTranslationLink('it_IT');
    $this->clickLink('IT');
    $this->assertText("Locale 'it_IT' data was copied from source for node Llamas are cool.");
    // Nothing was requested.
    $this->assertIdentical('es_MX', \Drupal::state()
      ->get('lingotek.added_target_locale'));

    // No link for Check status of the Italian translation.
    $this->assertNoLingotekCheckTargetStatusLink('it_IT');
    // And the Italian translation has been copied.
    $this->assertTargetStatus('IT', Lingotek::STATUS_UNTRACKED);

    // We can download the Spanish translation as a regular one.
    $this->assertLingotekDownloadTargetLink('es_MX');
    $this->clickLink('ES');
    $this->assertText('The translation of node Llamas are cool into es_MX has been downloaded.');
    $this->assertIdentical('es_MX', \Drupal::state()
      ->get('lingotek.downloaded_locale'));
  }

  /**
   * Tests that a node can be translated using the actions on the management page.
   */
  public function testNodeTranslationUsingActions() {
    // Login as admin.
    $this->drupalLogin($this->rootUser);

    // Create a node.
    $edit = [];
    $edit['title[0][value]'] = 'Llamas are cool';
    $edit['body[0][value]'] = 'Llamas are very cool';
    $edit['langcode[0][value]'] = 'en';
    $edit['lingotek_translation_management[lingotek_translation_profile]'] = 'custom_profile';
    $this->saveAndPublishNodeForm($edit);

    // Login as translation manager.
    $this->drupalLogin($this->translationManagerUser);

    $this->goToContentBulkManagementForm();

    // I can init the upload of content.
    $this->assertLingotekUploadLink();
    $key = $this->getBulkSelectionKey('en', 1);
    $edit = [
      $key => TRUE,
      $this->getBulkOperationFormName() => $this->getBulkOperationNameForUpload('node'),
    ];
    $this->drupalPostForm(NULL, $edit, $this->getApplyActionsButtonLabel());
    $this->assertIdentical('en_US', \Drupal::state()
      ->get('lingotek.uploaded_locale'));

    // I can check current status.
    $this->assertLingotekCheckSourceStatusLink();
    $key = $this->getBulkSelectionKey('en', 1);
    $edit = [
      $key => TRUE,
      $this->getBulkOperationFormName() => $this->getBulkOperationNameForCheckUpload('node'),
    ];
    $this->drupalPostForm(NULL, $edit, $this->getApplyActionsButtonLabel());

    // Request the Italian translation.
    $this->assertNoLingotekRequestTranslationLink('it_IT');
    $this->assertLingotekRequestCopyTranslationLink('it_IT');
    $key = $this->getBulkSelectionKey('en', 1);
    $edit = [
      $key => TRUE,
      $this->getBulkOperationFormName() => $this->getBulkOperationNameForRequestTranslation('it', 'node'),
    ];
    $this->drupalPostForm(NULL, $edit, $this->getApplyActionsButtonLabel());
    $this->assertIdentical(NULL, \Drupal::state()
      ->get('lingotek.added_target_locale'));

    // Check status of the Italian translation is no.
    $this->assertNoLingotekCheckTargetStatusLink('it_IT');

    // And the Italian translation has been copied.
    $this->assertTargetStatus('IT', Lingotek::STATUS_UNTRACKED);
  }

  /**
   * Tests that a node can be translated using the actions on the management page for multiple locales.
   */
  public function testNodeTranslationUsingActionsForMultipleLocales() {
    // Login as admin.
    $this->drupalLogin($this->rootUser);

    // Create a node.
    $edit = [];
    $edit['title[0][value]'] = 'Llamas are cool';
    $edit['body[0][value]'] = 'Llamas are very cool';
    $edit['langcode[0][value]'] = 'en';
    $edit['lingotek_translation_management[lingotek_translation_profile]'] = 'custom_profile';
    $this->saveAndPublishNodeForm($edit);

    $this->goToContentBulkManagementForm();

    // I can init the upload of content.
    $this->assertLingotekUploadLink();

    $key = $this->getBulkSelectionKey('en', 1);
    $edit = [
      $key => TRUE,
      $this->getBulkOperationFormName() => $this->getBulkOperationNameForUpload('node'),
    ];
    $this->drupalPostForm(NULL, $edit, $this->getApplyActionsButtonLabel());
    $this->assertIdentical('en_US', \Drupal::state()
      ->get('lingotek.uploaded_locale'));

    // I can check current status.
    $this->assertLingotekCheckSourceStatusLink();
    $key = $this->getBulkSelectionKey('en', 1);
    $edit = [
      $key => TRUE,
      $this->getBulkOperationFormName() => $this->getBulkOperationNameForCheckUpload('node'),
    ];
    $this->drupalPostForm(NULL, $edit, $this->getApplyActionsButtonLabel());

    // Request all translations.
    $this->assertNoLingotekRequestTranslationLink('it_IT');
    $this->assertLingotekRequestCopyTranslationLink('it_IT');
    $this->assertLingotekRequestTranslationLink('es_MX');

    $key = $this->getBulkSelectionKey('en', 1);
    $edit = [
      $key => TRUE,
      $this->getBulkOperationFormName() => $this->getBulkOperationNameForRequestTranslations('node'),
    ];
    $this->drupalPostForm(NULL, $edit, $this->getApplyActionsButtonLabel());
    $this->assertIdentical('es_MX', \Drupal::state()
      ->get('lingotek.added_target_locale'));

    // Check all statuses.
    $this->assertNoLingotekCheckTargetStatusLink('it_IT');
    $this->assertLingotekCheckTargetStatusLink('es_MX');
    // And the Italian translation has been copied.
    $this->assertTargetStatus('IT', Lingotek::STATUS_UNTRACKED);
  }

  /**
   * Asserts there is a link for requesting translation source copy for a given locale.
   *
   * @param string $locale
   *   The locale.
   * @param string $document_id
   *   The Lingotek document ID. Optional, defaults to 'dummy-document-hash-id'.
   * @param string $entity_type_id
   *   The entity type ID. Optional, defaults to node.
   * @param string|null $prefix
   *   Language prefix if any. Optional, defaults to NULL.
   */
  protected function assertLingotekRequestCopyTranslationLink($locale, $document_id = 'dummy-document-hash-id', $entity_type_id = 'node', $prefix = NULL, $destination_entity_type_id = NULL, $destination_entity_id = NULL) {
    $basepath = \Drupal::request()->getBasePath();
    $languagePrefix = ($prefix === NULL ? '' : '/' . $prefix);
    $destination_entity_type_id = $destination_entity_type_id ?: $entity_type_id;
    $href = $basepath . $languagePrefix . '/admin/lingotek/entity/copy-source-target/' . $document_id . '/' . $locale;
    if ($destination = $this->getDestination($destination_entity_type_id, $prefix)) {
      $href .= $destination;
    }
    $this->assertLinkByHref($href);
  }

  /**
   * Asserts there is NOT a link for requesting translation source copy for a given locale.
   *
   * @param string $locale
   *   The locale.
   * @param string $document_id
   *   The Lingotek document ID. Optional, defaults to 'dummy-document-hash-id'.
   * @param string $entity_type_id
   *   The entity type ID. Optional, defaults to node.
   * @param string|null $prefix
   *   Language prefix if any. Optional, defaults to NULL.
   */
  protected function assertNoLingotekRequestCopyTranslationLink($locale, $document_id = 'dummy-document-hash-id', $entity_type_id = 'node', $prefix = NULL, $destination_entity_type_id = NULL, $destination_entity_id = NULL) {
    $basepath = \Drupal::request()->getBasePath();
    $languagePrefix = ($prefix === NULL ? '' : '/' . $prefix);
    $destination_entity_type_id = $destination_entity_type_id ?: $entity_type_id;
    $href = $basepath . $languagePrefix . '/admin/lingotek/entity/copy-source-target/' . $document_id . '/' . $locale;
    if ($destination = $this->getDestination($destination_entity_type_id, $prefix)) {
      $href .= $destination;
    }
    $this->assertNoLinkByHref($href);
  }

  /**
   * Asserts there is NOT a link for checking the translation status for a given locale.
   *
   * @param string $locale
   *   The locale.
   * @param string $document_id
   *   The Lingotek document ID. Optional, defaults to 'dummy-document-hash-id'.
   * @param string $entity_type_id
   *   The entity type ID. Optional, defaults to node.
   * @param string|null $prefix
   *   Language prefix if any. Optional, defaults to NULL.
   */
  protected function assertNoLingotekCheckTargetStatusLink($locale, $document_id = 'dummy-document-hash-id', $entity_type_id = 'node', $prefix = NULL, $destination_entity_type_id = NULL, $destination_entity_id = NULL) {
    // ToDo: This is added to the parent class. Can be removed after.
    $basepath = \Drupal::request()->getBasePath();
    $languagePrefix = ($prefix === NULL ? '' : '/' . $prefix);
    $destination_entity_type_id = $destination_entity_type_id ?: $entity_type_id;
    $href = $basepath . $languagePrefix . '/admin/lingotek/entity/check_target/' . $document_id . '/' . $locale;
    if ($destination = $this->getDestination($destination_entity_type_id, $prefix)) {
      $href .= $destination;
    }
    $this->assertNoLinkByHref($href);
  }

}
