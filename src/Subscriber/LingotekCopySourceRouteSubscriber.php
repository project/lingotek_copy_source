<?php

namespace Drupal\lingotek_copy_source\Subscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber to replace lingotek controller notification callbacks so we can
 * prevent the request of translations and copy instead.
 */
class LingotekCopySourceRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('lingotek.notify')) {
      $route->setDefault('_controller', '\Drupal\lingotek_copy_source\Controller\LingotekNotificationController::endpoint');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes'];
    return $events;
  }

}
