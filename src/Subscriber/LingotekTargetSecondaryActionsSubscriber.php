<?php

namespace Drupal\lingotek_copy_source\Subscriber;

use Drupal\Core\Url;
use Drupal\lingotek\Event\TargetSecondaryActionsEvent;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\lingotek_copy_source\ProfileService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscriber to replace lingotek target secondary action links.
 */
class LingotekTargetSecondaryActionsSubscriber implements EventSubscriberInterface  {

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface
   */
  protected LingotekConfigurationServiceInterface $lingotekConfiguration;

  /**
   * The profile service.
   *
   * @var \Drupal\lingotek_copy_source\ProfileService
   */
  protected ProfileService $profileService;

  public function __construct(ProfileService $profile_service, LingotekConfigurationServiceInterface $lingotek_configuration) {
    $this->profileService = $profile_service;
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * Replacing links on secondary targets where we have to copy source.
   */
  public function onSecondaryTargets(TargetSecondaryActionsEvent $event) {
    $actions = &$event->getActions();
    $langcode = $event->getLangcode();

    $profile = $this->lingotekConfiguration->getEntityProfile($event->getEntity(), FALSE);
    if ($profile && $this->profileService->checkIfCopySource($profile, $langcode)) {
      // If there was a workbench link, we remove it
      unset($actions['workbench']);
      // We replace the title.
      $actions['request']['title'] = t('Copy from source');
      // We replace the link.
      $url = $actions['request']['url'];
      if ($url) {
        $routeParameters = $url->getRouteParameters();
        $routeOptions = $url->getOptions();
        $actions['request']['url'] = Url::fromRoute('lingotek_copy_source.content_entity_copy', $routeParameters, $routeOptions);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      TargetSecondaryActionsEvent::EVENT_NAME => 'onSecondaryTargets',
    ];
  }
}
