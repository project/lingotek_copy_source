<?php

namespace Drupal\lingotek_copy_source;

use Drupal\Core\Url;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ThemePreprocess implements ContainerInjectionInterface {

  /**
   * The profile service.
   *
   * @var \Drupal\lingotek_copy_source\ProfileService
   */
  protected $profileService;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  public function __construct(ProfileService $profile_service, LingotekConfigurationServiceInterface $lingotek_configuration) {
    $this->profileService = $profile_service;
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lingotek_copy_source.profile_service'),
      $container->get('lingotek.configuration')
    );
  }

  public function preprocessLingotekTargetStatuses(&$variables) {
    $entity = $variables['entity'];
    // Entity could be null if we were looking at interface translations.
    if ($entity !== NULL) {
      $profile = $this->lingotekConfiguration->getEntityProfile($entity, TRUE);
      $statuses = &$variables['statuses'];
      foreach ($statuses as $langcode => $status) {
        if ($this->profileService->checkIfCopySource($profile, $langcode)) {
          /** @var \Drupal\Core\Url $url */
          $url = &$statuses[$langcode]['url'];
          if ($url) {
            $routeParameters = $url->getRouteParameters();
            $routeOptions = $url->getOptions();
            $statuses[$langcode]['url'] = Url::fromRoute('lingotek_copy_source.content_entity_copy', $routeParameters, $routeOptions);
          }
        }
      }
    }
  }

}
