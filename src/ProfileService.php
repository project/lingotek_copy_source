<?php

namespace Drupal\lingotek_copy_source;

use Drupal\lingotek\LingotekProfileInterface;

class ProfileService {

  public function checkIfCopySource(LingotekProfileInterface $profile, $langcode) {
    // We cannot access protected arrays, so we need the raw data.
    $profileData = $profile->toArray();
    return (isset($profileData['language_overrides'][$langcode]) &&
      $profileData['language_overrides'][$langcode]['overrides'] === 'copy_source');
  }

}
