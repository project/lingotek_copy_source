<?php

namespace Drupal\lingotek_copy_source\Plugin\Action;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\lingotek\Exception\LingotekApiException;
use Drupal\lingotek\Exception\LingotekDocumentArchivedException;
use Drupal\lingotek\Exception\LingotekDocumentLockedException;
use Drupal\lingotek\Exception\LingotekPaymentRequiredException;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\lingotek\LingotekContentTranslationServiceInterface;
use Drupal\lingotek\Plugin\Action\LingotekContentEntityConfigurableActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Alternate request Lingotek translation of a content entity for one language or
 * copy from source.
 */
class RequestTranslationOrCopyLingotekAction extends LingotekContentEntityConfigurableActionBase {

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  /**
   * Constructs a new UploadToLingotekAction action.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface $language_locale_mapper
   *   The language-locale mapper.
   * @param \Drupal\lingotek\LingotekContentTranslationServiceInterface $translation_service
   *   The Lingotek content translation service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LanguageLocaleMapperInterface $language_locale_mapper, LingotekContentTranslationServiceInterface $translation_service, LingotekConfigurationServiceInterface $lingotek_configuration) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $language_locale_mapper, $translation_service);
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('lingotek.language_locale_mapper'),
      $container->get('lingotek.content_translation'),
      $container->get('lingotek.configuration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $result = FALSE;
    /** @var \Drupal\lingotek_copy_source\ProfileService $profileService */
    $profileService = \Drupal::service('lingotek_copy_source.profile_service');

    $configuration = $this->getConfiguration();
    $langcode = $configuration['language'];
    $profile = $this->lingotekConfiguration->getEntityProfile($entity, FALSE);
    if ($profileService->checkIfCopySource($profile, $langcode)) {
      $data = $this->translationService->getSourceData($entity);
      $this->translationService->saveTargetData($entity, $langcode, $data);
    }
    else {
      try {
        $locale = $this->languageLocaleMapper->getLocaleForLangcode($langcode);
        $result = $this->translationService->addTarget($entity, $locale);
      }
      catch (LingotekPaymentRequiredException $exception) {
        $this->messenger()
          ->addError(t('Community has been disabled. Please contact support@lingotek.com to re-enable your community.'));
      }
      catch (LingotekDocumentArchivedException $exception) {
        $this->messenger()
          ->addError(t('Document @entity_type %title has been archived. Please upload again.', [
            '@entity_type' => $entity->getEntityTypeId(),
            '%title' => $entity->label(),
          ]));
      }
      catch (LingotekDocumentLockedException $exception) {
        $this->messenger()
          ->addError(t('Document @entity_type %title has a new version. The document id has been updated for all future interactions. Please try again.', [
            '@entity_type' => $entity->getEntityTypeId(),
            '%title' => $entity->label(),
          ]));
      }
      catch (LingotekApiException $exception) {
        $this->messenger()
          ->addError(t('The request for @entity_type %title translation failed. Please try again.', [
            '@entity_type' => $entity->getEntityTypeId(),
            '@langcode' => $langcode,
            '%title' => $entity->label(),
          ]));
      }
    }
    return $result;
  }

}
