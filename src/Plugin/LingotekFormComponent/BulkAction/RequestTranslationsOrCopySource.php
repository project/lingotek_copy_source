<?php

namespace Drupal\lingotek_copy_source\Plugin\LingotekFormComponent\BulkAction;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\lingotek\Exception\LingotekApiException;
use Drupal\lingotek\Exception\LingotekDocumentArchivedException;
use Drupal\lingotek\Exception\LingotekDocumentLockedException;
use Drupal\lingotek\Exception\LingotekDocumentNotFoundException;
use Drupal\lingotek\Exception\LingotekPaymentRequiredException;
use Drupal\lingotek\Exception\LingotekProcessedWordsLimitException;
use Drupal\lingotek\FormComponent\LingotekFormComponentBulkActionExecutor;
use Drupal\lingotek\LanguageLocaleMapperInterface;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\lingotek\LingotekContentTranslationServiceInterface;
use Drupal\lingotek\Plugin\LingotekFormComponent\BulkAction\RequestTranslations;
use Drupal\lingotek_copy_source\ProfileService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides the request all translations operation for copying source when needed.
 */
class RequestTranslationsOrCopySource extends RequestTranslations {

  /**
   * The language-locale mapper.
   *
   * @var \Drupal\lingotek\LanguageLocaleMapperInterface
   */
  protected $languageLocaleMapper;

  /**
   * The profile service.
   *
   * @var \Drupal\lingotek_copy_source\ProfileService
   */
  protected $profileService;

  /**
   * DebugExport constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language_manager service.
   * @param \Drupal\lingotek\LingotekConfigurationServiceInterface $lingotek_configuration
   *   The lingotek.configuration service.
   * @param \Drupal\lingotek\LingotekContentTranslationServiceInterface $translation_service
   *   The lingotek.content_translation service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity_type.bundle.info service.
   * @param \Drupal\lingotek\LanguageLocaleMapperInterface $language_locale_mapper
   *   The language-locale mapper.
   * @param \Drupal\lingotek_copy_source\ProfileService $profile_service
   *   The profile service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, LingotekConfigurationServiceInterface $lingotek_configuration, LingotekContentTranslationServiceInterface $translation_service, EntityTypeBundleInfoInterface $entity_type_bundle_info, LanguageLocaleMapperInterface $language_locale_mapper, ProfileService $profile_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $language_manager, $lingotek_configuration, $translation_service, $entity_type_bundle_info);
    $this->languageLocaleMapper = $language_locale_mapper;
    $this->profileService = $profile_service;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('lingotek.configuration'),
      $container->get('lingotek.content_translation'),
      $container->get('entity_type.bundle.info'),
      $container->get('lingotek.language_locale_mapper'),
      $container->get('lingotek_copy_source.profile_service')
    );
  }

  public function executeSingle(ContentEntityInterface $entity, $options, LingotekFormComponentBulkActionExecutor $executor, array &$context) {
    $context['message'] = $this->t('Requesting translations for @type %label.', ['@type' => $entity->getEntityType()->getLabel(), '%label' => $entity->label()]);
    $bundleInfos = $this->entityTypeBundleInfo->getBundleInfo($entity->getEntityTypeId());
    if (!$entity->getEntityType()->isTranslatable() || !$bundleInfos[$entity->bundle()]['translatable']) {
      $this->messenger()->addWarning($this->t('Cannot request translations for @type %label. That @bundle_label is not enabled for translation.',
        ['@type' => $bundleInfos[$entity->bundle()]['label'], '%label' => $entity->label(), '@bundle_label' => $entity->getEntityType()->getBundleLabel()]));
      return FALSE;
    }
    if (!$this->lingotekConfiguration->isEnabled($entity->getEntityTypeId(), $entity->bundle())) {
      $this->messenger()->addWarning($this->t('Cannot request translations for @type %label. That @bundle_label is not enabled for Lingotek translation.',
        ['@type' => $bundleInfos[$entity->bundle()]['label'], '%label' => $entity->label(), '@bundle_label' => $entity->getEntityType()->getBundleLabel()]
      ));
      return FALSE;
    }
    if ($profile = $this->lingotekConfiguration->getEntityProfile($entity, FALSE)) {
      $target_languages = $this->languageManager->getLanguages();
      $target_languages = array_filter($target_languages, function (LanguageInterface $language) {
        $configLanguage = ConfigurableLanguage::load($language->getId());
        return $this->lingotekConfiguration->isLanguageEnabled($configLanguage);
      });
      $entity_langcode = $entity->getUntranslated()->language()->getId();
      foreach ($target_languages as $langcode => $language) {
        if ($langcode !== $entity_langcode) {
          if ($this->profileService->checkIfCopySource($profile, $langcode)) {
            $data = $this->translationService->getSourceData($entity);
            $this->translationService->saveTargetData($entity, $langcode, $data);
          }
          else {
            $locale = $this->languageLocaleMapper->getLocaleForLangcode($langcode);
            try {
              $this->translationService->addTarget($entity, $locale);
            }
            catch (LingotekDocumentNotFoundException $exc) {
              $this->messenger()->addError($this->t('Document @entity_type %title was not found. Please upload again.', ['@entity_type' => $entity->getEntityTypeId(), '%title' => $entity->label()]));
              return FALSE;
            }
            catch (LingotekPaymentRequiredException $exception) {
              $this->messenger()
                ->addError(t('Community has been disabled. Please contact support@lingotek.com to re-enable your community.'));
            }
            catch (LingotekDocumentArchivedException $exception) {
              $this->messenger()
                ->addError(t('Document @entity_type %title has been archived. Please upload again.', [
                  '@entity_type' => $entity->getEntityTypeId(),
                  '%title' => $entity->label(),
                ]));
            }
            catch (LingotekDocumentLockedException $exception) {
              $this->messenger()
                ->addError(t('Document @entity_type %title has a new version. The document id has been updated for all future interactions. Please try again.', [
                  '@entity_type' => $entity->getEntityTypeId(),
                  '%title' => $entity->label(),
                ]));
            }
            catch (LingotekProcessedWordsLimitException $exception) {
              $this->messenger()->addError($this->t('Processed word limit exceeded. Please contact your local administrator or Lingotek Client Success (<a href=":link">@mail</a>) for assistance.', [':link' => 'mailto:sales@lingotek.com', '@mail' => 'sales@lingotek.com']));
              return FALSE;
            }
            catch (LingotekApiException $exception) {
              $this->messenger()
                ->addError(t('The request for @entity_type %title translation failed. Please try again.', [
                  '@entity_type' => $entity->getEntityTypeId(),
                  '%title' => $entity->label(),
                ]));
            }
          }
        }
      }
    }
    else {
      $this->messenger()->addWarning($this->t('The @type %label has no profile assigned so it was not processed.',
        ['@type' => $bundleInfos[$entity->bundle()]['label'], '%label' => $entity->label()]));
      return FALSE;
    }
    return TRUE;
  }

}
