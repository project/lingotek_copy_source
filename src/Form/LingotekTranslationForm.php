<?php

namespace Drupal\lingotek_copy_source\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\lingotek\LingotekConfigurationServiceInterface;
use Drupal\lingotek_copy_source\ProfileService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LingotekTranslationForm implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The profile service.
   *
   * @var \Drupal\lingotek_copy_source\ProfileService
   */
  protected $profileService;

  /**
   * The Lingotek configuration service.
   *
   * @var \Drupal\lingotek\LingotekConfigurationServiceInterface
   */
  protected $lingotekConfiguration;

  public function __construct(ProfileService $profile_service, LingotekConfigurationServiceInterface $lingotek_configuration) {
    $this->profileService = $profile_service;
    $this->lingotekConfiguration = $lingotek_configuration;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lingotek_copy_source.profile_service'),
      $container->get('lingotek.configuration')
    );
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  public function formAlter(&$form, FormStateInterface $form_state, $form_id) {
    $formStorage = $form_state->getStorage();
    $entity = $formStorage['entity'];
    /** @var \Drupal\lingotek\LingotekProfileInterface $profile */
    $profile = $this->lingotekConfiguration->getEntityProfile($entity, TRUE);
    foreach ($form['languages']['#options'] as $langcode => $rowData) {
      if ($this->profileService->checkIfCopySource($profile, $langcode)) {
        $links = $rowData[3]['data']['#links'];
        if (isset($links['request translation'])) {
          /** @var \Drupal\Core\Url $url */
          $url = $form['languages']['#options'][$langcode][3]['data']['#links']['request translation']['url'];
          if ($url) {
            $routeParameters = $url->getRouteParameters();
            $routeOptions = $url->getOptions();
            $form['languages']['#options'][$langcode][3]['data']['#links']['request translation']['url'] =
              Url::fromRoute('lingotek_copy_source.content_entity_copy', $routeParameters, $routeOptions);
          }
        }
      }
    }
  }

}
