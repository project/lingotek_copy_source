<?php

namespace Drupal\lingotek_copy_source\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\lingotek_copy_source\ProfileService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LingotekProfileForm implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The profile service.
   *
   * @var \Drupal\lingotek_copy_source\ProfileService
   */
  protected $profileService;

  public function __construct(ProfileService $profile_service) {
    $this->profileService = $profile_service;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lingotek_copy_source.profile_service')
    );
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   */
  public function formAlter(&$form, FormStateInterface $form_state, $form_id) {
    /** @var \Drupal\lingotek\LingotekProfileInterface $profile */
    $profile = $form_state->getFormObject()->getEntity();
    foreach (Element::children($form['language_overrides']) as $langcode) {
      $form['language_overrides'][$langcode]['overrides']['#options']['copy_source'] = $this->t('Copy source');
      if ($this->profileService->checkIfCopySource($profile, $langcode)) {
        $form['language_overrides'][$langcode]['overrides']['#default_value'] = 'copy_source';
      }
    }
  }

}
