<?php

namespace Drupal\lingotek_copy_source\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\lingotek\Lingotek;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\lingotek\Controller\LingotekNotificationController as OriginalLingotekNotificationController;

/**
 * Returns responses for lingotek callbacks, but intercepts when translations
 * should be copied from source.
 */
class LingotekNotificationController extends OriginalLingotekNotificationController {

  public function endpoint(Request $request) {
    $callbacks = ['document_uploaded', 'document_updated'];
    $type = $request->query->get('type');
    if (!in_array($type, $callbacks)) {
      return parent::endpoint($request);
    }

    /** @var \Drupal\lingotek_copy_source\ProfileService $profileService */
    $profileService = \Drupal::service('lingotek_copy_source.profile_service');
    $translation_service = $this->lingotekContentTranslation;

    $request_method = $request->getMethod();
    $http_status_code = Response::HTTP_ACCEPTED;
    $result = [];
    $messages = [];
    $security_token = $request->query->get('security_token');
    if ($security_token == 1) {
      $http_status_code = Response::HTTP_ACCEPTED;
    }
    switch ($type) {
      // a document has uploaded and imported successfully for document_id
      case 'document_uploaded':
        $entity = $this->getEntity($request->query->get('document_id'));
        /** @var \Drupal\lingotek\Entity\LingotekProfile $profile */
        $profile = $this->getProfile($entity);
        if ($entity) {
          if ($entity instanceof ConfigEntityInterface) {
            $translation_service = $this->lingotekConfigTranslation;
          }
          elseif (is_string($entity)) {
            $translation_service = $this->lingotekInterfaceTranslation;
          }
          $http_status_code = Response::HTTP_OK;
          $translation_service->setSourceStatus($entity, Lingotek::STATUS_CURRENT);
          $languages = [];
          $copiedLanguages = [];
          $target_languages = $this->languageManager()->getLanguages();
          $target_languages = array_filter($target_languages, function (LanguageInterface $language) {
            $configLanguage = ConfigurableLanguage::load($language->getId());
            return $this->lingotekConfiguration->isLanguageEnabled($configLanguage);
          });
          foreach ($target_languages as $target_language) {
            if ($profile && $profileService->checkIfCopySource($profile, $target_language->getId())) {
              // We have a second parameter for seeing if we already visited the
              // entity. Ensuring it is not empty will ensure the intelligence
              // metadata is not included which calls render() and would create
              // here a leak of cacheability metadata and therefor an error.
              $visitedHack = ['whatever-which-is-not-empty-will-prevent-intelligence-metadata-to-be-included'];
              $data = $translation_service->getSourceData($entity, $visitedHack);
              $translation_service->saveTargetData($entity, $target_language->getId(), $data);
              $copiedLanguages[] = $target_language->getId();
            }
            elseif ($profile && $profile->hasAutomaticRequestForTarget($target_language->getId())) {
              $target_locale = $this->languageLocaleMapper->getLocaleForLangcode($target_language->getId());
              if ($translation_service->addTarget($entity, $target_locale)) {
                $languages[] = $target_language->getId();
              }
            }
          }
          $result['request_translations'] = $languages;
          $result['copy_source_translations'] = $copiedLanguages;
        }
        else {
          $http_status_code = Response::HTTP_NO_CONTENT;
          $messages[] = "Document not found.";
        }
        break;

      case 'document_updated':
        $entity = $this->getEntity($request->query->get('document_id'));
        /** @var \Drupal\lingotek\Entity\LingotekProfile $profile */
        $profile = $this->getProfile($entity);
        if ($entity) {
          if ($entity instanceof ConfigEntityInterface) {
            $translation_service = $this->lingotekConfigTranslation;
          }
          elseif (is_string($entity)) {
            $translation_service = $this->lingotekInterfaceTranslation;
          }
          $http_status_code = Response::HTTP_OK;
          $translation_service->setSourceStatus($entity, Lingotek::STATUS_CURRENT);
          $languages = [];
          $copiedLanguages = [];
          $target_languages = $this->languageManager()->getLanguages();
          $target_languages = array_filter($target_languages, function (LanguageInterface $language) {
            $configLanguage = ConfigurableLanguage::load($language->getId());
            return $this->lingotekConfiguration->isLanguageEnabled($configLanguage);
          });
          foreach ($target_languages as $target_language) {
            if ($profile && $profileService->checkIfCopySource($profile, $target_language->getId())) {
              // We have a second parameter for seeing if we already visited the
              // entity. Ensuring it is not empty will ensure the intelligence
              // metadata is not included which calls render() and would create
              // here a leak of cacheability metadata and therefor an error.
              $visitedHack = ['whatever-which-is-not-empty-will-prevent-intelligence-metadata-to-be-included'];
              $data = $translation_service->getSourceData($entity, $visitedHack);
              $translation_service->saveTargetData($entity, $target_language->getId(), $data);
              $copiedLanguages[] = $target_language->getId();
            }
            elseif ($profile && $profile->hasAutomaticRequestForTarget($target_language->getId())) {
              $target_locale = $this->languageLocaleMapper->getLocaleForLangcode($target_language->getId());
              if ($translation_service->addTarget($entity, $target_locale)) {
                $languages[] = $target_language->getId();
              }
            }
          }
          $result['request_translations'] = $languages;
          $result['copy_source_translations'] = $copiedLanguages;
        }
        else {
          $http_status_code = Response::HTTP_NO_CONTENT;
          $messages[] = "Document not found.";
        }
        break;
    }

    $response = [
      'service' => 'notify',
      'method' => $request_method,
      'params' => $request->query->all(),
      'result' => $result,
      'messages' => $messages,
    ];

    $response = CacheableJsonResponse::create($response, $http_status_code)
      ->setMaxAge(0)
      ->setSharedMaxAge(0);
    $response->getCacheableMetadata()->addCacheContexts(['url.query_args']);
    return $response;
  }

}
