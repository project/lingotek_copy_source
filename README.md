# Lingotek Copy Source

## Introduction

If you have different locales for the same language, this modules allows you to copy from the source,
**preventing to request translations** to Lingotek when there are minor edits needed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/lingotek_copy_source

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/lingotek_copy_source

 * For more information on the Lingotek module, visit the project page:
   https://www.drupal.org/project/lingotek


## Requirements

Lingotek module (https://www.drupal.org/project/lingotek)

## Installation

* Normal module installation procedure. See
https://www.drupal.org/documentation/install/modules-themes/modules-8


## Configuration

Edit a Lingotek translation profile in _Lingotek Settings_, expand the _Target Language Specific_ settings, scroll to
the language/locale you want to copy (e.g. en-GB), and on the dropdown change "Default" to "Copy Source".

<img src="https://www.drupal.org/files/lingotek_copy_source.png" alt="Lingotek Copy Source settings" />

## Maintainers

Current maintainers:
 * Christian López Espínola (penyaskito) - https://www.drupal.org/u/penyaskito
